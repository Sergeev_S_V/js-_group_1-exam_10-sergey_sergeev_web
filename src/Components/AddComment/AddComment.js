import React, {Component} from 'react';
import Button from "../UI/Button/Button";

class AddComment extends Component {

  state = {
    name: '',
    comment: '',
    postId: '',
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  submitFormHandler = event => {
    event.preventDefault();

    let comment = {
      author: this.state.name,
      comment: this.state.comment,
      postId: this.props.postId,
    };

    this.props.onSubmit(comment);

    this.setState({comment: ''});
  };

  render() {
    return(
      <div className='AddComment'>
        <form action="" onSubmit={this.submitFormHandler}>
          <div>
            <label htmlFor="">Name</label>
            <input type="text" name='name'
                   onChange={this.inputChangeHandler}
                   value={this.state.name}
            />
          </div>
          <div>
            <label htmlFor="">Comment</label>
            <textarea name="comment" cols="20" rows="5" required
                      onChange={this.inputChangeHandler}
                      value={this.state.comment}
            />
          </div>
          <Button text='Add' submit/>
        </form>
      </div>
    );
  }
}

export default AddComment;