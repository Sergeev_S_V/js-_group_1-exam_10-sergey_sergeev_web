import React, {Component} from 'react';
import Button from "../UI/Button/Button";
import {connect} from "react-redux";
import {sendNewPost} from "../../store/actions";

class AddNewPost extends Component {

  state = {
    title: '',
    content: '',
    image: '',
  };

  fileChangeHandler = event => {
    this.setState({[event.target.name]: event.target.files[0]});
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);

    this.setState({title: '', content: '', image: ''});
    this.props.history.push('/');
  };

  render() {
    return(
      <div>
        <form action="" onSubmit={this.submitFormHandler}>
          <div className='Title-Form'>
            <label htmlFor="">Title</label>
            <input type="text" name='title' required
                   onChange={this.inputChangeHandler}
                   value={this.state.title}
            />
          </div>
          <div>
            <label htmlFor="">Content</label>
            <textarea name="content" id="" cols="20" rows="5" required
                      onChange={this.inputChangeHandler}
                      value={this.state.content}
            />
          </div>
          <div>
            <label htmlFor="">Choose file</label>
            <input type="file" name='image'
                   onChange={this.fileChangeHandler}
            />
          </div>
          <Button text='Save' submit='submit'/>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmit: post => dispatch(sendNewPost(post))
  }
};

export default connect(null, mapDispatchToProps)(AddNewPost);