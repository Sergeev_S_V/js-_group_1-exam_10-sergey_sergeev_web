import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';

const Header = () => (
  <header>
    <h4><NavLink to='/' exact>News</NavLink></h4>
    {/*<NavLink to='/add-new-contact' exact><button>Add new contact</button></NavLink>*/}
  </header>
);

export default Header;