import React from 'react';

const Button = props => (
  <button type={props.submit ? 'submit' : null} onClick={props.clicked}>
    {props.text}
  </button>
);

export default Button;