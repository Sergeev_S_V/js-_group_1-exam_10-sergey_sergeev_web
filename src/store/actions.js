import axios from '../axios-api';
import {
  FETCH_POST_ERROR, SUCCESS_ADD_COMMENT, SUCCESS_FETCH_ALL_NEWS, SUCCESS_FETCH_COMMENTS,
  SUCCESS_FETCH_POST
} from "./actionTypes";

export const sendNewPost = post => dispatch => {
  axios.post('/news', post)
    .then(resp => {
      console.log(resp);
    });
};

export const fetchAllNews = () => dispatch => {
  axios.get('/news')
    .then(resp => {
      dispatch(successFetchAllNews(resp.data));
    })
};

export const successFetchAllNews = news => {
  return {type: SUCCESS_FETCH_ALL_NEWS, news};
};

export const deletePost = id => dispatch => {
  axios.delete(`/news/${id}`)
    .then(() => {
      dispatch(fetchAllNews());
    })
};

export const fetchPostError = error => {
  return {type: FETCH_POST_ERROR, error};
};

export const fetchPost = id => dispatch => {
  axios.get(`/news/${id}`)
    .then(res => {
      if (res.data.length > 0) {
        const id = res.data[0].id;
        dispatch(successFetchPost(res.data, id))
      } else {
        return null;
      }
    },
      error => fetchPostError(error));
};

export const successFetchPost = (post, id) => {
  return {type: SUCCESS_FETCH_POST, post, id};
};

export const addComment = comment => dispatch => {
  axios.post('/comments', comment)
    .then(res => {
      dispatch(successAddComment(res.data));
    })
};

export const successAddComment = comment => {
  return {type: SUCCESS_ADD_COMMENT, comment};
};

export const fetchComments = (id) => dispatch => {
  axios.get(`/comments?news_id=${id}`)
    .then(res => {
      dispatch(successFetchComments(res.data));
    });
};

export const successFetchComments = postComments => {
  return {type: SUCCESS_FETCH_COMMENTS, postComments};
};

export const removeComment = (id, postId) => dispatch => {
  axios.delete(`/comments/${id}`)
    .then(() => dispatch(fetchComments(postId)));
};