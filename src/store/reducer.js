import {
  DELETE_POST, FETCH_POST_ERROR, SUCCESS_ADD_COMMENT, SUCCESS_FETCH_ALL_NEWS, SUCCESS_FETCH_COMMENTS,
  SUCCESS_FETCH_POST
} from "./actionTypes";

const initialState = {
  news: [],
  lookingPost: null,
  postComments: [],
  lookingPostId: '',
  error: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_FETCH_ALL_NEWS:
      return {...state, news: action.news};
    case DELETE_POST:
      let news = [...state.news];
      const index = news.findIndex(post => post.id === action.id);
      news.slice(index);
      return {...state, news};
    case SUCCESS_FETCH_POST:
      return {...state, lookingPost: action.post, lookingPostId: action.id};
    case SUCCESS_ADD_COMMENT: // ?
      return {...state, postComments: state.postComments.concat(action.comment)};
    case SUCCESS_FETCH_COMMENTS:
      return {...state, postComments: action.postComments};
    case FETCH_POST_ERROR:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;