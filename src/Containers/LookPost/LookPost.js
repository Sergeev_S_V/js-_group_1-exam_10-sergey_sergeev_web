import React, {Component} from 'react';
import {connect} from "react-redux";
import {addComment, fetchComments, fetchPost, removeComment} from "../../store/actions";
import PostContent from "./PostContent/PostContent";
import AddComment from "../../Components/AddComment/AddComment";
import Comment from "./Comment/Comment";
import './LookPost.css';

class LookPost extends Component {

  componentDidMount() {
    this.props.onFetchPost(this.props.match.params.id);
    this.props.onFetchComments(this.props.match.params.id);
  };

  render() {
    if (this.props.error) {
      return <div>Not found</div>
    } else {
    return(
      <div>
        {this.props.lookingPost
          ? this.props.lookingPost
            .map(post => {
              return <PostContent
                key={post.id}
                title={post.title}
                content={post.content}
                date={post.date}
              />
            })
          : <div>Post is empty</div>
        }
        <div className='Comments'>
          <h5>Comments</h5>
          {this.props.postComments && this.props.lookingPost
            ? this.props.postComments
              .map(comment => (
                <Comment key={comment.id}
                         author={comment.author}
                         comment={comment.comment}
                         remove={() => this.props.onRemoveComment(comment.id, this.props.match.params.id)}
            />

            ))
            : <div>No comments</div>
          }
        </div>

        <AddComment postId={this.props.lookingPostId}
                    onSubmit={this.props.onAddComment}
        />
      </div>
    );
    }
  }
}

const mapStateToProps = state => {
  return {
    lookingPost: state.lookingPost,
    lookingPostId: state.lookingPostId,
    postComments: state.postComments,
    error: state.error
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchPost: id => dispatch(fetchPost(id)),
    onFetchComments: id => dispatch(fetchComments(id)),
    onAddComment: comment => dispatch(addComment(comment)),
    onRemoveComment: (id, postId) => dispatch(removeComment(id, postId))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(LookPost);