import React from 'react';

const PostContent = props => (
  <div className='PostContent'>
    <h4>{props.title}</h4>
    <span>{props.date}</span>
    <p>{props.content}</p>
  </div>
);

export default PostContent;