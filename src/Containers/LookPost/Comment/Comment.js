import React from 'react';
import Button from "../../../Components/UI/Button/Button";
import './Comment.css';

const Comment = props => (
  <div className='Comment'>
    <p><strong>{props.author} </strong>wrote: {props.comment}</p>
    <Button text='Delete' clicked={props.remove}/>
  </div>
);

export default Comment;