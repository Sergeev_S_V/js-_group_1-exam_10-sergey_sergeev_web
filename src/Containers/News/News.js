import React, {Component} from 'react'
import {connect} from "react-redux";
import {deletePost, fetchAllNews} from "../../store/actions";
import Post from "./Post/Post";
import Button from "../../Components/UI/Button/Button";
import NavLink from "react-router-dom/es/NavLink";
import './News.css';

class News extends Component {

  componentDidMount() {
    this.props.onFetchAllNews();
  };

  render() {
    return(
      <div className='News'>
        <div className='News-Top-Nav'>
          <h3 className='News-Title'>Posts</h3>
          <NavLink className='News-AddNewPost-Btn' to='/add-new-post'>
            <Button text='Add new post'/>
          </NavLink>
        </div>

        <div className='Posts'>
          {
            this.props.news
              ? this.props.news.map(post => {
                return <Post key={post.id}
                             title={post.title}
                             date={post.date}
                             image={post.image}
                             path={post.id}
                             remove={() => this.props.onDeletePost(post.id)}
                />
              })
              : null
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    news: state.news
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchAllNews: () => dispatch(fetchAllNews()),
    onDeletePost: (id) => dispatch(deletePost(id)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(News);