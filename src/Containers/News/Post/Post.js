import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

import Button from "../../../Components/UI/Button/Button";
import './Post.css';

class Post extends Component {
  render() {
    return(
      <div className='Post'>
        {this.props.image
          ? <img className='Post-Image'
                 src={'http://localhost:8000/uploads/' + this.props.image} alt=""/>
          : null
        }
        <div>
          <h5>{this.props.title}</h5>
          <span>{this.props.date}</span>
          <NavLink to={`/news/${this.props.path}`} exact><Button text='Read Full Post >>'/></NavLink>
        </div>
        <Button text='Delete' clicked={this.props.remove}/>
      </div>
    );
  }
}

export default Post;