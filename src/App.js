import React, { Component } from 'react';
import {Route, Switch} from "react-router";

import './App.css';
import Layout from "./Components/Layout/Layout";
import News from "./Containers/News/News";
import AddNewPost from "./Components/AddNewPost/AddNewPost";
import LookPost from "./Containers/LookPost/LookPost";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Layout>
          <Switch>
            <Route path='/' exact component={News}/>
            <Route path='/news' exact component={News}/>
            <Route path='/add-new-post' exact component={AddNewPost}/>
            <Route path='/news/:id' exact component={LookPost}/>
          </Switch>
        </Layout>
      </div>
    );
  }
}

export default App;
